<?php

namespace App\Weapons;

use App\Weapon;

/**
 * Created by PhpStorm.
 * User: bengraf
 * Date: 15.01.18
 * Time: 18:47
 */
class AK47 extends Weapon
{
    public $model = 'AK-47';
    public $maxAmmo = 30;
    public $shootingQueue = 3;
}
