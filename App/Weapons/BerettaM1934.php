<?php

namespace App\Weapons;

use App\Weapon;

/**
 * User: bengraf
 * Date: 15.01.18
 * Time: 18:47
 */
class BerettaM1934 extends Weapon
{
    public $model = 'Beretta M1934';
    public $maxAmmo = 7;
    public $shootingQueue = 1;
}
