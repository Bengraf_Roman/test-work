<?php

namespace App;

abstract class Weapon
{
    /**
     * @var string
     */
    protected $model;
    /**
     * @var int
     */
    protected $maxAmmo;

    /**
     * @var integer
     */
    protected $ammo;

    /**
     * @var string звук выстрела
     */
    protected $bang = 'BANG!';
    /**
     * @var string зыук клика
     */
    protected $click = 'Click.';

    /**
     * @var string обрыв строки
     */
    protected $stringBreak = "\n";
    /**
     * @var int
     */
    protected $shootingQueue;

    /**
     * Weapon constructor.
     * @param int $loadAmmo
     * @throws \Exception
     */
    public function __construct(int $loadAmmo)
    {
        $this->stringBreak = php_sapi_name() == "cli" ? "\n" : "<br>";
        if ($loadAmmo > $this->getMaxAmmo()) {
            throw new \Exception(
                sprintf("overlimit of maximum possible ammo capacity of %s for %s", $this->maxAmmo, $this->getModel())
            );
        }
        $loadAmmo < 0 ?: $this->ammo += $loadAmmo;
    }

    /**
     * Модель оружия
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Максимальный заряд
     * @return int
     */
    public function getMaxAmmo()
    {
        return $this->maxAmmo;
    }

    /**
     * Текущий заряд
     * @return int
     */
    public function getAmmo()
    {
        return $this->ammo;
    }

    /**
     * Огонь!
     * @return mixed
     */
    public function fire()
    {
        for ($i = 0; $i < $this->shootingQueue; $i++) {
            if (!$this->spendAmmo()) {
                $this->makeClick();
                $this->makeEnd();
                return;
            }
            $this->makeBang();
        }
        $this->makeEnd();
        return;
    }

    /**
     * Перезарядка
     */
    public function reload()
    {
        $this->ammo = $this->getMaxAmmo();
    }

    /**
     * Тратим патрон
     * @return bool|int
     */
    protected function spendAmmo()
    {
        return $this->ammo ? $this->ammo-- : false;
    }

    /**
     * Звучит выстрел
     */
    protected function makeBang()
    {
        echo $this->bang . " ";
    }

    /**
     * Звучит клик
     */
    protected function makeClick()
    {
        echo $this->click . " ";
    }

    /**
     * Новая строка
     */
    protected function makeEnd()
    {
        echo $this->stringBreak;
    }
}
