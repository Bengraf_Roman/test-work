### Легкие тестовые задания
#### Задача №1 (на понимание ООП)
Необходимо написать 3 класса, описывающие оружие и их базовое поведение,
удовлетворяющие следующим требованиям:
- должен быть написан один родительский класс Weapon и два его дочерних
класса AK47 и BerettaM1934
- класс Weapon должен быть абстратным
- в задаче предполагается, что AK47 стреляет короткими очередями сразу по 3
патрона
- код не должен выдавать никаких ошибок при исполнении, кроме случаев
ручного вызова ошибок программистом в коде
- дочерние классы должны принимать текущее количество патронов, которое
передается в конструктор при создании класса
- если в конструктор передано количество патронов, которое “не помещается” в
магазин оружия, то должна выдаваться ошибка “overlimit of maximum possible
ammo capacity of (здесь число) for this model of weapon”

##### Тестирующий код (адаптирован исполниетелем):
```php
<?php
include "autoload.php";

use App\Weapon;
use App\Weapons\AK47;
use App\Weapons\BerettaM1934;

class ShootingRange
{
    public $stringBreak = "\n";

    function testWeapon(Weapon $weapon)
    {
        echo 'Model: ' . $weapon->getModel() . $this->stringBreak;
        echo 'Max ammo: ' . $weapon->getMaxAmmo() . $this->stringBreak;
        echo 'Current ammo: ' . $weapon->getAmmo() . $this->stringBreak;
        $weapon->fire();
        echo 'Current ammo: ' . $weapon->getAmmo() . $this->stringBreak;
        $weapon->fire();
        $weapon->fire();
        $weapon->fire();
        echo 'Current ammo: ' . $weapon->getAmmo() . $this->stringBreak;
        $weapon->reload();
        echo 'Current ammo: ' . $weapon->getAmmo() . $this->stringBreak;
    }
}

$shootingRange = new ShootingRange();
$shootingRange->testWeapon(new AK47(8));
echo $shootingRange->stringBreak;
$shootingRange->testWeapon(new BerettaM1934(2));
echo $shootingRange->stringBreak;
$shootingRange->testWeapon(new BerettaM1934(8));

```

Результат выполнения тестирующего кода должен быть следующий:
```
Model: AK-47
Max ammo: 30
Current ammo: 8
BANG! BANG! BANG!
Current ammo: 5
BANG! BANG! BANG!
BANG! BANG! Click.
Click.
Current ammo: 0
Current ammo: 30
Model: Beretta M1934
Max ammo: 7
Current ammo: 2
BANG!
Current ammo: 1
BANG!
Click.
Click.
Current ammo: 0
Current ammo: 7

Fatal error: Uncaught exception 'Exception' with message
'overlimit of maximum possible ammo capacity of 7 for this model
of weapon' in ...
```
#### Задача №2 (на алгоритмы).
Существуют 2 текстовых файла, содержащих в себе ФИО людей. В качестве
разделителя для ФИО в файлах используется символ переноса на новую строку (\n),
т.е. каждое ФИО находится в новой строке. Данные в файлах упорядочены по
алфавиту. Необходимо описать алгоритм создания третьего файла, в котором должны
присутствовать данные из первых двух файлов. Данные в третьем файле также
должны быть упорядочены по алфавиту. Для решения задачи не нужно писать
программный код. Достаточно описать алгоритм своими словами / на псевдокоде /
составить блок-схему.

#### 1
 - Получить строку из файла 1.
 - Получить стркоу из файла 2.
 - Объединить строку 1 и 2 ($1+\n+$2)
 - Извлечь из строки массив значение
 - Сортировка массива (например merge sort)
 - "склеивание строки"
 
#### 2
 Наборы уже отсортированы. "Берем" первый элемент набора 1, и набора 2. Сравниванием  - больший пишем в файл 3, в файле с большим значением перемещаем курсор до следующего знака "\n". 
 Повторояем процедуру пока указатель не будет указывать на 1, последнюю ФИО в наборе.
