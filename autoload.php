<?php
/**
 * Типа автозагрузка
 */
spl_autoload_register(
    function ($pClassName) {
        include(__DIR__ . DIRECTORY_SEPARATOR . str_replace("\\", "/", $pClassName . ".php"));
    }
);
ini_set('display_errors', '1');
error_reporting(E_ALL);
