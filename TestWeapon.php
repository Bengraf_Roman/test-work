<?php
include "autoload.php";

use App\Weapon;
use App\Weapons\AK47;
use App\Weapons\BerettaM1934;

class ShootingRange
{
    public $stringBreak = "\n";

    function testWeapon(Weapon $weapon)
    {
        echo 'Model: ' . $weapon->getModel() . $this->stringBreak;
        echo 'Max ammo: ' . $weapon->getMaxAmmo() . $this->stringBreak;
        echo 'Current ammo: ' . $weapon->getAmmo() . $this->stringBreak;
        $weapon->fire();
        echo 'Current ammo: ' . $weapon->getAmmo() . $this->stringBreak;
        $weapon->fire();
        $weapon->fire();
        $weapon->fire();
        echo 'Current ammo: ' . $weapon->getAmmo() . $this->stringBreak;
        $weapon->reload();
        echo 'Current ammo: ' . $weapon->getAmmo() . $this->stringBreak;
    }
}

$shootingRange = new ShootingRange();
$shootingRange->testWeapon(new AK47(8));
echo $shootingRange->stringBreak;
$shootingRange->testWeapon(new BerettaM1934(2));
echo $shootingRange->stringBreak;
$shootingRange->testWeapon(new BerettaM1934(8));
